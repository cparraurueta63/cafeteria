<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->integer('total_venta')->nullable(false);
            $table->integer('cantidad')->nullable(false);
            $table->unsignedBigInteger('id_producto')->nullable();            
            $table->foreign('id_producto')
                ->references('id')
                ->on('productos')
                ->onUpdate('set null')
                ->onDelete('set null');                   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
};
