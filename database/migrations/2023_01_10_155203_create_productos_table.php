<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable(false);
            $table->text('referencia')->nullable(false);
            $table->integer('precio')->nullable(false);
            $table->integer('peso')->nullable(false);
            $table->integer('stock')->nullable(false)->default(0);
            $table->unsignedBigInteger('id_categoria')->nullable();
            
            $table->foreign('id_categoria')
                ->references('id')
                ->on('categorias')
                ->onUpdate('set null')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
};
