<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Venta extends Model
{
    use HasFactory;

    public function producto()
    {
        return $this->belongsTo(Producto::class,'id_producto');
    }

    public static function mayorVenta()
    {
        $producto = DB::select('SELECT p.*, SUM(v.cantidad) as unidades
                                FROM ventas AS v
                                INNER JOIN productos AS p ON p.id = v.id_producto
                                GROUP BY p.id
                                ORDER BY SUM(v.cantidad) DESC LIMIT 1');
        return $producto[0];
    }
}
