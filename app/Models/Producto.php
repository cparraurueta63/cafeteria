<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Producto extends Model
{
    use HasFactory;

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'id_categoria');
    }

    public static function mayorStock()
    {
        $producto = DB::select('SELECT * 
                                FROM productos
                                ORDER BY stock DESC
                                LIMIT 1');
        return $producto[0];
    }
}
