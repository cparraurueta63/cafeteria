<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Http\Requests\StoreProductoRequest;
use App\Http\Requests\UpdateProductoRequest;
use App\Models\Categoria;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::with('categoria')->get();
        return view('productos.index', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::all();
        return view('productos.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductoRequest $request)
    {
        $producto = new Producto();
        $producto->nombre = $request->nombre;
        $producto->referencia = $request->referencia;
        $producto->precio = $request->precio;
        $producto->peso = $request->peso;
        $producto->stock = $request->stock;
        $producto->id_categoria = $request->id_categoria;
        if ($producto->save()) {
            return redirect()->route('productos.index')->with(['type' => 'info', 'message' => 'Exito, el registro fue guardado correctamente']);
        } else {
            return redirect()->route('productos.index')->with(['type' => 'danger', 'message' => 'Error, el registro fue guardado incorrectamente']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::find($id);
        $categorias = Categoria::all();
        return view('productos.edit', compact('categorias', 'producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProductoRequest  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductoRequest $request, $id)
    {
        $producto = Producto::find($id);
        $producto->nombre = $request->nombre;
        $producto->referencia = $request->referencia;
        $producto->precio = $request->precio;
        $producto->peso = $request->peso;
        $producto->stock = $request->stock;
        $producto->id_categoria = $request->id_categoria;


        if ($producto->save()) {
            return redirect()->route('productos.index')->with(['type' => 'info', 'message' => 'Exito, el registro fue actualizado correctamente']);
        } else {
            return redirect()->route('productos.index')->with(['type' => 'danger', 'message' => 'Error al actualizar registro']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::find($id);
        if ($producto->delete()) {
            return redirect()->route('productos.index')->with(['type' => 'info', 'message' => 'Exito, el registro fue eliminado correctamente']);
        } else {
            return redirect()->route('productos.index')->with(['type' => 'danger', 'message' => 'Error al eliminar registro']);
        }
    }   
}
