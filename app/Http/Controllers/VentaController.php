<?php

namespace App\Http\Controllers;

use App\Models\Venta;
use App\Http\Requests\StoreVentaRequest;
use App\Http\Requests\UpdateVentaRequest;
use App\Models\Producto;
use Illuminate\Http\Request;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ventas = Venta::with('producto')->get();
        $producto = null;
        if ($request->id) {
            $producto = Producto::where('id', $request->id)->first();
        }
        return view('vender.index', compact('ventas', 'producto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreVentaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVentaRequest $request)
    {
        $venta = new Venta();
        $producto = Producto::find($request->_id);
        if ($producto->stock > 0) {
            if ($producto->stock >= $request->_cantidad) {
                $venta->id_producto = $request->_id;
                $venta->cantidad = $request->_cantidad;
                $venta->total_venta = $request->_total;
                if ($venta->save()) {
                    $producto->stock = $producto->stock - $request->_cantidad;
                    $producto->save();
                    return redirect()->route('vender.index')->with(['type' => 'info', 'message' => 'Venta realizada']);
                } else {
                    return redirect()->route('vender.index')->with(['type' => 'danger', 'message' => 'Error al realizar venta']);
                }
            } else {
                return redirect()->back()->with(['type' => 'danger', 'message' => 'No hay unidades sufucientes para la venta']);
            }
        } else {
            return redirect()->back()->with(['type' => 'danger', 'message' => 'No hay unidades disponibles']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function show(Venta $venta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function edit(Venta $venta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateVentaRequest  $request
     * @param  \App\Models\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVentaRequest $request, Venta $venta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Venta $venta)
    {
        //
    }
}
