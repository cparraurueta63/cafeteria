<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:255',            
            'referencia' => 'required',            
            'precio' => 'required|numeric|min:0',
            'peso' => 'required|numeric|min:0',            
            'stock' => 'required|numeric|min:0',            
            'id_categoria' => 'required',            
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es obligatorio.',
            'nombre.max' => 'El nombre debe tener menos de 256 caracteres.',
            'referencia.required' => 'La referencia es obligatoria',

            'precio.required' => 'La precio es obligatorio',
            'precio.numeric' => 'El precio debe ser un numero',
            'precio.min' => 'El precio debe ser un numero positivo',

            'peso.required' => 'El peso es obligatorio',
            'peso.numeric' => 'El peso debe ser un numero',
            'peso.min' => 'El peso debe ser un numero positivo',
            
            'stock.required' => 'El stock es obligatorio',
            'stock.numeric' => 'El stock debe ser un numero',
            'stock.min' => 'El stock debe ser un numero positivo',

            'id_categoria.required' => 'La categoria es obligatoria',
        ];
    }
}
