@extends('adminlte::page')

@section('title', 'Vender')
@section('content')
<br>
@if (Session::has('message'))
<div class="alert alert-{{Session::get('type')}}">
    {{Session::get('message')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="row">
    <div class="col-6">
        <div class="card">
            <div class="card-header">
                Realizar una venta

            </div>
            <div class="card-body">
                <form class="form-inline" action="{{route('vender.index')}}" method="GET">
                    <div class="form-group mx-sm-3 mb-2">
                        <input type="number" name="id" class="form-control" id="id" placeholder="Ingrese ID" value="{{old('id')}}">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Buscar Producto</button>
                </form>
                <div class="card card-primary mt-2">
                    <div class="card-header">
                        <h3 class="card-title">Producto a vender</h3>
                    </div>

                    <form action="{{route('vender.store')}}" method="POST">
                        @csrf
                        <div class="card-body">
                            @if($producto == null)
                            <div class="form-group">
                                <label>No hay producto para mostrar.</label>
                            </div>
                            @else
                            <input type="hidden" name="_total" id="_total" value="{{$producto->precio * 1}}">
                            <input type="hidden" name="_id" value="{{$producto->id}}">
                            <div class="form-group">
                                <label for="nombre">ID</label>
                                <input type="text" class="form-control" id="id" name="id" disabled value="{{$producto->id}}">
                            </div>
                            <div class="form-group">
                                <label for="nombre">Nombre Producto</label>
                                <input type="text" class="form-control" id="nombre" disabled value="{{$producto->nombre}}">
                            </div>
                            <div class="form-group">
                                <label for="nombre">Stock</label>
                                <input type="text" class="form-control" id="stock" disabled value="{{$producto->stock}}">
                            </div>
                            <div class="form-group">
                                <label for="peso">Peso</label>
                                <input type="number" class="form-control" id="peso" disabled value="{{$producto->peso}}">
                            </div>
                            <div class="form-group">
                                <label for="nombre">Precio</label>
                                <input type="number" class="form-control" id="precio" disabled value="{{$producto->precio}}">
                            </div>
                            <div class="form-group">
                                <label for="nombre">Cantidad</label>
                                <input type="number" class="form-control" id="_cantidad" value="1" name="_cantidad">
                            </div>
                            <div class="form-group">
                                <label for="nombre">Total</label>
                                <input type="number" class="form-control" id="total" name="total" disabled value="{{old('total', $producto->precio * 1)}}">
                            </div>
                            @endif
                        </div>
                        @if($producto != null)
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Vender</button>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>

    </div>
    <div class="col-6">
        <div class="card">
            <div class="card-header">
                Todas las ventas
            </div>
            <div class="card-body">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th>Id Venta</th>
                                <th>Id Producto</th>
                                <th>Nombre Producto</th>
                                <th>Cantidad</th>
                                <th>Valor de Venta</th>
                                <th>Fecha de venta</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($ventas as $venta)
                            <tr>
                                <td>{{$venta->id}}</td>
                                <td>{{$venta->id_producto}}</td>
                                <td>{{isset($venta->producto->nombre) ? $venta->producto->nombre : ''}}</td>
                                <td>{{$venta->cantidad}}</td>
                                <td>{{$venta->total_venta}}</td>
                                <td>{{ date_format($venta->created_at, 'd-m-Y') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>

@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    let cantidad = document.getElementById("_cantidad");
    cantidad.addEventListener('keyup', cambiarTotal);

    function cambiarTotal() {
        if (document.getElementById("_cantidad").value != "") {
            const precio = document.getElementById("precio").value;
            let cantidad = document.getElementById("_cantidad").value;
            document.getElementById("total").value = parseInt(precio) * parseInt(cantidad);
            document.getElementById("_total").value = document.getElementById("total").value;
        } else {
            document.getElementById("total").value = "";
            document.getElementById("_total").value = "";
        }
    }
</script>
@stop