<div class="modal" tabindex="-1" role="dialog" id="product-{{$producto->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eliminar producto "{{$producto->nombre}}"</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>¿Está seguro que desea eliminar el producto?</p>
            </div>
            <div class="modal-footer">
                <form action="{{route('productos.destroy', $producto->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-danger">Si</button>
                </form>
            </div>
        </div>
    </div>
</div>