@extends('adminlte::page')

@section('title', 'Productos')

@section('content_header')
<h1>Productos</h1>
@stop

@section('content')
@if (Session::has('message'))
<div class="alert alert-{{Session::get('type')}}">
    {{Session::get('message')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="/productos/create" class="btn btn-primary">Crear Producto</a>
                </h3>

            </div>

            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre de producto</th>
                            <th>Referencia</th>
                            <th>Precio</th>
                            <th>Peso</th>
                            <th>Stock</th>
                            <th>Categoría</th>
                            <th>Fecha de creación</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($productos as $producto)
                        <tr>
                            <td>{{$producto->id}}</td>
                            <td>{{$producto->nombre}}</td>
                            <td>{{$producto->referencia}}</td>
                            <td>{{$producto->precio}}</td>
                            <td>{{$producto->peso}}</td>
                            <td>{{$producto->stock}}</td>
                            <td>{{isset($producto->categoria->nombre) ? $producto->categoria->nombre : ''}}</td>
                            <td>{{ date_format($producto->created_at, 'd-m-Y') }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="{{ route('productos.edit', $producto) }}" class="btn btn-primary">Editar</a>
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#product-{{$producto->id}}">Eliminar</button>
                                </div>
                            </td>
                        </tr>
                        @include('productos.delete')
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');
</script>
@stop