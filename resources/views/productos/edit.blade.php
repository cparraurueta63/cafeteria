@extends('adminlte::page')

@section('title', 'Editar Productos')


@section('content')
<div class="card card-primary mt-4">
    <div class="card-header">
        <h3 class="card-title">Editar Producto</h3>
    </div>
    <form action="{{ route('productos.update', $producto->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="form-group">
                <label for="nombre">Nombre del producto</label>
                <input required type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese el nombre del producto" 
                value="{{ old('nombre', $producto->nombre) }}">
            </div>
            <div class="form-group">
                <label for="referencia">Referencia</label>
                <input required type="text" name="referencia" class="form-control" id="referencia" placeholder="Referencia"
                value="{{ old('referencia', $producto->referencia) }}">
            </div>
            <div class="form-group">
                <label for="precio">Precio</label>
                <input required type="number" name="precio" class="form-control" id="precio" placeholder="Precio"
                value="{{ old('precio', $producto->precio) }}">
            </div>
            <div class="form-group">
                <label for="peso">Peso</label>
                <input required type="number" name="peso" class="form-control" id="peso" placeholder="Peso"
                value="{{ old('peso', $producto->peso) }}">
            </div>
            <div class="form-group">
                <label for="stock">Stock</label>
                <input required type="number" name="stock" class="form-control" id="peso" placeholder="Stock"
                value="{{ old('stock', $producto->stock) }}">
            </div>
            <div class="form-group">
                <label for="categoria">Categoria</label>
                <select required name="id_categoria" class="form-control" id="id_categoria" value="{{ old('id_categoria', $producto->id_categoria) }}">
                    <option value="">--Selecione la categoria--</option>
                    @foreach ($categorias as $categoria)
                    <option value="{{$categoria->id}}" {{$producto->id_categoria == $categoria->id ? 'selected': ''}}>{{$categoria->nombre}}</option>
                    @endforeach
                </select>
            </div>


        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Actualizar</button>
        </div>
    </form>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');
</script>
@stop