@extends('adminlte::page')

@section('title', 'Crear Productos')



@section('content')
<div class="card card-primary mt-4">
    <div class="card-header">
        <h3 class="card-title">Registrar Producto</h3>
    </div>
    <form action="{{ route('productos.store') }}" method="POST">
        @csrf
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="form-group">
                <label for="nombre">Nombre del producto</label>
                <input required type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese el nombre del producto" 
                value="{{ old('nombre') }}">
            </div>
            <div class="form-group">
                <label for="referencia">Referencia</label>
                <input required type="text" name="referencia" class="form-control" id="referencia" placeholder="Referencia"
                value="{{ old('referencia') }}">
            </div>
            <div class="form-group">
                <label for="precio">Precio</label>
                <input required type="number" name="precio" class="form-control" id="precio" placeholder="Precio"
                value="{{ old('precio') }}">
            </div>
            <div class="form-group">
                <label for="peso">Peso</label>
                <input required type="number" name="peso" class="form-control" id="peso" placeholder="Peso"
                value="{{ old('peso') }}">
            </div>
            <div class="form-group">
                <label for="stock">Stock</label>
                <input required type="number" name="stock" class="form-control" id="peso" placeholder="Stock"
                value="{{ old('stock') }}">
            </div>
            <div class="form-group">
                <label for="categoria">Categoria</label>
                <select required name="id_categoria" class="form-control" id="id_categoria" value="{{ old('id_categoria') }}">
                    <option value="">--Selecione la categoria--</option>
                    @foreach ($categorias as $categoria)
                    <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Gurdar</button>
        </div>
    </form>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');
</script>
@stop