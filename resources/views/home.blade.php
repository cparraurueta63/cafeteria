@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Inicio</h1>
@stop

@section('content')
<div class="callout callout-success">
    <h5>Producto que más stock tiene</h5>
    <p>Nombre: {{$productoStock->nombre}}</p>
    <p>Stock: {{$productoStock->stock}}</p>
    <h5>Consulta</h5>
    <p>SELECT *
        FROM productos
        ORDER BY stock DESC
        LIMIT 1
    </p>
</div>
<div class="callout callout-success">
    <h5>Producto más vendido</h5>
    <p>Nombre: {{$productoVenta->nombre}}</p>
    <p>Unidades vendidas: {{$productoVenta->unidades}}</p>
    <h5>Consulta</h5>
    <p>SELECT p.*, SUM(v.cantidad) AS unidades        
        FROM ventas AS v
        INNER JOIN productos AS p ON p.id = v.id_producto
        GROUP BY p.id
        ORDER BY SUM(v.cantidad) DESC LIMIT 1
    </p>
</div>


@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');
</script>
@stop